## OpenCL Buddhabrot Renderer

Avg. 8.72G iterations / s on GTX1060

![](https://raw.githubusercontent.com/FredericaBernkastel/OpenCL_Buddhabrot/master/doc/anti-buddhabrot.jpg)
![](https://raw.githubusercontent.com/FredericaBernkastel/OpenCL_Buddhabrot/master/doc/buddhabrot.jpg)
![](https://raw.githubusercontent.com/FredericaBernkastel/OpenCL_Buddhabrot/master/doc/anti-buddhabrot%20(z%20%3D%20z%20%5E%204%20%2B%20c)%2C%20maxiter%3D512.jpg)
![](https://raw.githubusercontent.com/FredericaBernkastel/OpenCL_Buddhabrot/master/doc/z%3Dz%5E1.55%2Bc%2C%20iter%3D146649%20128%20128%204%20512%2C%20t%3D1321s%20%401080.jpg)
