﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OpenCLTemplate;
using Cloo;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;

namespace std
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public void DrawImage(ref Bitmap bmp)
        {
            pictureBox1.Image = bmp;
        }

        public void UpdateUI(long t, long t0, uint iter, uint maxiter, uint frame, uint maxframe)
        {
            string pattern = @"OpenCL Buddhabrot [elapsed: {0}s / {1}ms] [iter: {2} / {3}]" + (frame != 0 ? " [frame: {4} / {5}]" : "");
            string elapsed = String.Format(pattern,
                (t / 1000f).ToString("0.000"),
                (t - t0).ToString("000"),
                iter.ToString("000"),
                maxiter.ToString("000"),
                frame.ToString("0000"),
                maxframe.ToString("0000")
            );

            this.Text = elapsed;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.renderer.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!Program.renderer.Init(this))
                Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!Program.renderer.UI_AbortAttempt())
                MessageBox.Show("Unable to stop renderer.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Program.renderer.CompileKernel();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
            if(saveFileDialog1.FileName != "")
            {
                Program.renderer.bmp.Save(saveFileDialog1.FileName);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Program.renderer.Reset();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Program.renderer.Test(),
                "Test Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
