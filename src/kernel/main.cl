﻿#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable


//2 component vector to hold the real and imaginary parts of a complex number:
typedef float2 cfloat;
#define I ((cfloat)(0.0, 1.0))

#define E .0000001f

bool fEqual(float x, float y)
{
    return (x+E > y && x-E < y);
}

__constant float aspectRatio = 16.0/9.0;

/* render window (pixel) */
__constant cfloat windowSize = (cfloat)(3, 3);
__constant cfloat windowCenter = (cfloat)(0, 0);

/* projection offset and zoom */
__constant cfloat screenSize = (cfloat)( 1 * 16.0 / 9.0, 1 );
__constant cfloat screenCenter = (cfloat)(0,0);

/* Enable atomics with global memory (2x slowdown) */
__constant bool SyncWrite = true;

typedef float4 color;

inline color UInt32ToARGB(uint pixel){
  color result;
  result.w = (pixel & 0xFF) / (float)0xFF;
  result.z = ((pixel >> 0x08) & 0xFF) / (float)0xFF;
  result.y = ((pixel >> 0x10) & 0xFF) / (float)0xFF;
  result.x = ((pixel >> 0x18) & 0xFF) / (float)0xFF;
  
  return result;
}

inline uint ARGBToUInt32(color pixel){
  return convert_uint(pixel.w * (float)0xFF) | (convert_uint(pixel.z * (float)0xFF) << 0x08) | (convert_uint(pixel.y * (float)0xFF) << 0x10) | (convert_uint(pixel.x * (float)0xFF) << 0x18);
}

inline color HSL2ARGB(float3 HSL){
  
  float3 rgb = (float3)(0,0,0);
  
  //calculate chroma
  float chroma = (1.0f - fabs( (float)(2.0f * HSL.z - 1.0f) )) * HSL.y;
  float H = HSL.x / 60.0f;
  float x = chroma * (1.0f - fabs( fmod(H, 2.0f) - 1.0f ));
  
  switch((int)H)
  {
      case 0:
          rgb = (float3)(chroma, x, 0);
          break;
      case 1:
          rgb = (float3)(x, chroma, 0);
          break;
      case 2:
          rgb = (float3)(0, chroma, x);
          break;
      case 3:
          rgb = (float3)(0, x, chroma);
          break;
      case 4:
          rgb = (float3)(x, 0, chroma);
          break;
      case 5:
          rgb = (float3)(chroma, 0, x);
          break;
      default:
          rgb = (float3)(0, 0, 0);    
  }
  
  rgb += HSL.z - .5f * chroma;
  return (color)(1, rgb.x, rgb.y, rgb.z); 
}
inline float3 ARGB2HSL(color color){
  
    //calculate chroma
    float M = max(max(color.y, color.z), color.w);
    float m = min(min(color.y, color.z), color.w);
    float chroma = M - m;
    float lightness = (M + m)/2.0f;
    float saturation = chroma / (1.0f - fabs(2.0f * lightness - 1.0f));

    float hue = 0;
    if (fEqual(color.y, M))
        hue = fmod((color.z - color.w) / chroma, 6.0f);
    if (fEqual(color.z, M))
        hue = (((color.w - color.y)) / chroma) + 2;
    if (fEqual(color.w, M))
        hue = (((color.y - color.z)) / chroma) + 4;
    hue *= 60.0f;
    
    return (float3)(hue, saturation, lightness);
}

/*
 * Return Real (Imaginary) component of complex number:
 */
inline float  real(cfloat a){
     return a.x;
}
inline float  imag(cfloat a){
     return a.y;
}

/*
 * Get the modulus of a complex number (its length):
 */
inline float cabs(cfloat z){
    return hypot(z.x, z.y);
}

inline float cabs_squared(cfloat z){
    return z.x * z.x + z.y * z.y;
}

/*
 * Get the argument of a complex number (its angle):
 * http://en.wikipedia.org/wiki/Complex_number#Absolute_value_and_argument
 */
inline float carg(cfloat a){
    if(a.x > 0){
        return atan(a.y / a.x);

    }else if(a.x < 0 && a.y >= 0){
        return atan(a.y / a.x) + M_PI;

    }else if(a.x < 0 && a.y < 0){
        return atan(a.y / a.x) - M_PI;

    }else if(a.x == 0 && a.y > 0){
        return M_PI/2;

    }else if(a.x == 0 && a.y < 0){
        return -M_PI/2;

    }else{
        return 0;
    }
}

/*
 * Multiply two complex numbers:
 *
 */
inline cfloat cmul(cfloat a, cfloat b){
    return (cfloat)( a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

inline cfloat csqr(cfloat a){
    return (cfloat)( a.x * a.x - a.y * a.y, 2 * a.x * a.y);
}

inline cfloat cexp(cfloat z) {
	return (cfloat)(exp(z.x) * cos(z.y), exp(z.x) * sin(z.y));
}

inline cfloat clog(cfloat a) {
	float b = atan2(a.y, a.x);
	if (b > 0.0) b = b - 2.0 * M_PI;
	return (cfloat)( log(length(a)), b );
}

inline cfloat cpower2(cfloat z, cfloat w) {
	return cexp(cmul(clog(z), w));
}

inline cfloat cpow(cfloat z, cfloat w){
  
  float logr = log(hypot(real(z), imag(z)));
  float logi = atan2(imag(z), real(z));

  float x = exp(logr * real(w) - logi * imag(w));
  float y = logr * imag(w) + logi * real(w);
  
  float cosy;
  float siny = sincos(y, &cosy);
  cfloat result = (cfloat)(x * cosy, x * siny);
  
  if(isnan(result.x) || isnan(result.y))
    result = HUGE_VALF;
  return result;
}

inline cfloat cpowr(cfloat z, float w)      
{ 
  float logr = log(hypot(real(z), imag(z))); 
  float logi = atan2(imag(z), real(z)); 
  float x = exp(logr * w); 
  float y = logi * w; 
  
  float cosy; 
  float siny = sincos(y, &cosy); 
  
  return (cfloat)(x * cosy, x * siny);
} 


inline uint2 coords_Window2Screen(cfloat z, cfloat size){
  return convert_uint2(((z + windowCenter + windowSize / (float)2) / windowSize * size * (cfloat)(windowSize.x / windowSize.y, 1)) * (cfloat)(1, -1) + (cfloat)(0, size.y)) - 1;
}

inline cfloat coords_Normal2Window(cfloat z){
  return z * windowSize - windowSize / (float)2 - windowCenter;
}


inline bool coords_testOverflow(uint2 pixel, uint2 size){
  return  (pixel.x >= 0) && (pixel.x < size.x) &&
          (pixel.y >= 0) && (pixel.y < size.y);
}

inline bool clipping_rect(float4 lrtb, cfloat v){
  //float4 lrtb = (float4)(center - size / 2, center + size / 2);
  return (v.x > lrtb.x) && (v.x < lrtb.y) && (v.y > lrtb.w) && (v.y < lrtb.z);
}

inline float2 rand(uint2 state)
{
    const float2 invMaxInt = (float2) (1.0f/4294967296.0f, 1.0f/4294967296.0f);
    uint x = state.x * 17 + state.y * 13123;
    state.x = (x<<13) ^ x;
    state.y ^= (x<<7);

    uint2 tmp = (uint2)
    ( (x * (x * x * 15731 + 74323) + 871483),
      (x * (x * x * 13734 + 37828) + 234234) );

    return convert_float2(tmp) * invMaxInt;
}

void atom_add_float(volatile global float *source, const float operand) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
 
    do {
        prevVal.floatVal = *source;
        newVal.floatVal = prevVal.floatVal + operand;
    } while (atomic_cmpxchg((volatile global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}
void atom_max_float(volatile global float *source, const float operand) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
 
    do {
        prevVal.floatVal = *source;
        newVal.floatVal = max(prevVal.floatVal, operand);
    } while (atomic_cmpxchg((volatile global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

__kernel void main(
    __global float * anim,
    __global uint * iter,
    __global long * frequency_buffer, 
    __global int * size, 
    __global float * random, 
    __global float * random2, 
    __global long * frequency_max,
    __global float * color_buffer,
    __global float * color_max
  )
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  int layer_z = get_global_id(2);
  
  int dimm_x = get_global_size(0);
  int dimm_y = get_global_size(1);
  int dimm_z = get_global_size(2);
    
  cfloat z = 1e-12;
  cfloat pixel;
  float iterM = (float)iter[0] / (float)iter[1];
  //pixel = coords_Normal2Window(rand((uint2)(x + random[layer_z], y + random[layer_z])));
  pixel = coords_Normal2Window(((cfloat)(random[y * dimm_y + x], random[dimm_x * dimm_y + y * dimm_y + x]) + (cfloat)(random2[0], random2[1])) / 2.0f);
  //if(!clipping_rect((float4)(-3, 0.31, 1.5, -1.5), pixel))
  //  return;
  uint2 _size = (uint2)(size[0], size[1]);
  
  uint maxiter = 256;
  cfloat path[256];
  float power = anim[0];
  int i = 0;
  for(i = 0; (i < maxiter) && (cabs_squared(z) < 4); i++){
    z = cpowr(z, power);
    if(isnan(z.x)) {break;}
    z = z + pixel;
    path[i] = z;
  }
  if(i == maxiter){
    for(int j = 0; j < i; j++){
      if(cabs(path[j]) > E){
        uint2 coords = coords_Window2Screen((path[j] + screenCenter) / screenSize, (cfloat)(_size.x, _size.y));
        if(coords_testOverflow(coords, _size)){
          uint index = coords.y * _size.x + coords.x;

          float color = pow(cabs(pixel), cabs(pixel) * -2.0f);
          float color_old = color_buffer[index];
          
          if(SyncWrite){
            atom_inc(&frequency_buffer[index]);
            atom_max(&frequency_max[0], frequency_buffer[index]);
            atom_add_float(&color_buffer[index], color);
            atom_max_float(&color_max[0], color_old);
          } else {
            frequency_buffer[index] += 1;
            frequency_max[0] = max(frequency_max[0], frequency_buffer[index]);
            color_buffer[index] = color + color_old;
            color_max[0] = max(color_max[0], color_old);
          }
        }
      }
    }
  }
}

__kernel void DrawImage(
    __global uint * image, 
    __global long * frequency_buffer, 
    __global int * size, 
    __global long * frequency_max,
    __global float * color_buffer,
    __global float * color_max,    
    __global uint * palette
  )
{
  int x = get_global_id(0);
  int y = get_global_id(1);
  
  if(frequency_max[0] > 0){
    float frequency = (float)frequency_buffer[y * size[0] + x];
    float alpha = 1.25 * log((frequency + 1)) / log(((float)frequency_max[0] + 1));
    float gamma = 0.4;
    float colormax = (float)color_max[0];
    float color = color_buffer[y * size[0] + x];
    uint colorIndex = convert_uint(cbrt((color) + 1) / cbrt((colormax) + 1) * 255 * 0.8 + 48) % 256;
    
    //float3 colorHSL = ARGB2HSL(UInt32ToARGB(palette[colorIndex]));
    //colorHSL.z = min(pow(alpha, 1 / gamma), (float)1);
    
    //image[y * size[0] + x] = ARGBToUInt32(HSL2ARGB(ARGB2HSL(UInt32ToARGB(palette[colorIndex])) * (float3)(1,1,min(pow(alpha, 1 / gamma), (float)1)))) | 0xFF000000;
    image[y * size[0] + x] = ARGBToUInt32(UInt32ToARGB(palette[colorIndex]) * min(pow(alpha, 1 / gamma), (float)1)) | 0xFF000000;
    //image[y * size[0] + x] = ARGBToUInt32((color)(1,1,1,1) * min(pow(alpha, 1 / gamma), (float)1)) | 0xFF000000;
  }
}

__kernel void TestKernel(
    __global float * test_result
)
{
  cfloat t0 = cpowr((cfloat)(2, 1), 2); // 3 + 4i
  test_result[0] = real(t0); test_result[1] = imag(t0);
  cfloat t1 = cpow((cfloat)(2, 1), (cfloat)(2, 0)); // 3 + 4i
  test_result[2] = real(t1); test_result[3] = imag(t1);
  cfloat t2 = cpow((cfloat)(3.7, 0.5), (cfloat)(2, 1)); // -0.185587 + 12.1865i
  test_result[4] = real(t2); test_result[5] = imag(t2);
  cfloat t3 = (cfloat)(0.78, 0.5);
  cfloat t4 = (cfloat)(0.78, 0.5);
  cfloat t5 = (cfloat)(0.78, 0.5);
  for(int i = 0; i < 8; i++){
    t3 = csqr(t3);
    t4 = cpowr(t4, 2);
    t5 = cpow(t5, (cfloat)(2, 0));
  }
  test_result[6] = real(t3); test_result[7] = imag(t3);
  test_result[8] = real(t4); test_result[9] = imag(t4);
  test_result[10] = real(t5); test_result[11] = imag(t5);
}