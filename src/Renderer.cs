﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using OpenCLTemplate;
using Cloo;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;


namespace std
{
    class Renderer
    {
        // callbacks to the user interface
        private MainForm UI;


        // target render size
        static readonly int[] image_size = { 1920, 1080 };

        // iterations count
        static readonly int maxiter = 1 << 10;

        // GPU Workers
        // time of each iteration of the kernel shouldn't exceed 50ms
        static int[] workers = new int[3] { 256, 256, 1 };
        static int[] workers_DrawImage = new int[2] { image_size[0], image_size[1] };

        // Amount of iterations till preview image will be updated
        static int DrawInterval = 256;

        static uint[] img = new uint[image_size[0] * image_size[1]];
        static long[] frequency_buffer = new long[image_size[0] * image_size[1]];
        static long[] frequency_max = new long[] { 0 };
        static float[] color_buffer = new float[image_size[0] * image_size[1]];
        static float color_max = 0;
        static uint[] palette = new uint[256];
        static float[] random_cache = new float[256 * 256 * 2];
        static float[] test_result = new float[12];

        static float[] anim = new float[]
        {
            1, 2, 3, 4
        };

        public Bitmap bmp;

        // OpenCL kernels
        static CLCalc.Program.Kernel buddhabrot;
        static CLCalc.Program.Kernel DrawImage;
        static CLCalc.Program.Kernel TestKernel;

        static CLCalc.Program.Variable arg1;
        static CLCalc.Program.Variable _anim;
        static CLCalc.Program.Variable _iter;
        static CLCalc.Program.Variable _frequency_buffer;
        static CLCalc.Program.Variable _frequency_max;
        static CLCalc.Program.Variable _size;
        static CLCalc.Program.Variable _random_cache;
        static CLCalc.Program.Variable _random2;
        static CLCalc.Program.Variable _color_buffer;
        static CLCalc.Program.Variable _color_max;
        static CLCalc.Program.Variable _palette;
        static CLCalc.Program.Variable _test_result;

        // kernel arguments
        static CLCalc.Program.Variable[] args_buddhabrot;
        static CLCalc.Program.Variable[] args_DrawImage;
        static CLCalc.Program.Variable[] args_TestKernel;

        // main renderer thread
        Thread OpenCLThr;

        // events
        public delegate void OpenCLThr_EventHandler();
        public event OpenCLThr_EventHandler _evt_Abort;
        public event OpenCLThr_EventHandler _evt_DrawImage;


        Stopwatch stopWatch = new Stopwatch();

        public class _kernel_argv : List<KeyValuePair<CLCalc.Program.Variable, object>> { };

        public bool Init(MainForm form)
        {
            UI = form;

            CLCalc.InitCL(ComputeDeviceTypes.Gpu);

            List<ComputeDevice> L = CLCalc.CLDevices;

            // default device id
            CLCalc.Program.DefaultCQ = 0;


            if (!CompileKernel())
            {
                return false;
            }

            bmp = new Bitmap(image_size[0], image_size[1], image_size[0] * 4,
                PixelFormat.Format32bppArgb,
                Marshal.UnsafeAddrOfPinnedArrayElement(img, 0));

            string __palette = @"
                00ACFC00A9FC00A5FC00A2FC009EFC009BFC0097FC0094FC
                0091FC008DFC008AFC0086FC0083FC007FFC007CFC0079FC
                0075FC0072FC006EFC006BFC0067FC0064FC0061FC005DFC
                005AFC0056FC0053FC004FFC004CFC0049FC0045FC0042FC
                003EFC003BFC0037FC0034FC0031FC002DFC002AFC0026FC
                0023FC001FFC001CFC0019FC0015FC0012FC000EFC000BFC
                0007FC0004FC0205F90406F70607F40808F20A09EF0C0AEC
                0E0AEA100BE7120CE5140DE2160EE0180FDD1A10DA1C11D8
                1E12D52013D32214D02415CD2616CB2817C82A17C62C18C3
                2E19C0311ABE331BBB351CB9371DB6391EB33B1FB13D20AE
                3F21AC4122A94323A74523A44724A149259F4B269C4D279A
                4F2897512994532A92552B8F572C8D592D8A5B2E875D2F85
                5F308261308063317D65327B6733786934756B35736D3670
                6F376E71386B733968753A66773B63793C617B3D5E7D3D5B
                7F3E59813F5683405485415187424F89434C8B44498D4547
                8F464492474294483F96493C98493A9A4A379C4B359E4C32
                A04D2FA24E2DA44F2AA65028A85125AA5222AC5320AE541D
                B0551BB25618B45616B65713B85810BA590EBC5A0BBE5B09
                C05C06C15E06C26106C36406C46606C56806C66B05C76E05
                C87005C87205C97505CA7805CB7A05CC7C05CD7F05CE8205
                CF8404D08604D18904D28C04D38E04D49004D59304D69604
                D69804D79A04D89D04D9A003DAA203DBA403DCA703DDAA03
                DEAC03DFAE03E0B103E1B403E2B603E3B803E4BB02E5BE02
                E6C002E6C202E7C502E8C802E9CA02EACC02EBCF02ECD202
                EDD402EED601EFD901F0DC01F1DE01F2E001F3E301F4E601
                F4E801F5EA01F6ED01F7F000F8F200F9F400FAF700FBFA00
                FCFC00FCFC68FCFC6BFCFC6EFCFC72FCFC75FCFC78FCFC7B
                FDFD7EFDFD82FDFD85FDFD88FDFD8BFDFD8EFDFD92FDFD95
                FDFD98FDFD9BFDFD9EFDFDA2FDFDA5FDFDA8FEFEABFEFEAE
                FEFEB2FEFEB5FEFEB8FEFEBBFEFEBEFEFEC2FEFEC5FEFEC8
                FEFECBFEFECEFEFED2FFFFD5FFFFD8FFFFDBFFFFDEFFFFE2
                FFFFE5FFFFE8DBF3EBB6E7EE92DBF16DD0F349C4F624B8F9".
              Replace(Environment.NewLine, "").Replace(" ", "");

            for (int i = 0; i < 256; i++)
                palette[i] = uint.Parse(__palette.Substring(i * 6, 6), System.Globalization.NumberStyles.AllowHexSpecifier);

            arg1 = new CLCalc.Program.Variable(img);
            _anim = new CLCalc.Program.Variable(new float[] { 0 });
            _iter = new CLCalc.Program.Variable(new uint[] { 0, 0 });
            _frequency_buffer = new CLCalc.Program.Variable(frequency_buffer);
            _frequency_max = new CLCalc.Program.Variable(frequency_max);
            _size = new CLCalc.Program.Variable(image_size);
            _random_cache = new CLCalc.Program.Variable(random_cache);
            _random2 = new CLCalc.Program.Variable(new float[] { 0, 0 });
            _color_buffer = new CLCalc.Program.Variable(color_buffer);
            _color_max = new CLCalc.Program.Variable(new float[] { color_max });
            _palette = new CLCalc.Program.Variable(palette);
            _test_result = new CLCalc.Program.Variable(test_result);
            args_buddhabrot = new CLCalc.Program.Variable[] { _anim, _iter, _frequency_buffer, _size, _random_cache, _random2, _frequency_max, _color_buffer, _color_max };
            args_DrawImage = new CLCalc.Program.Variable[] { arg1, _frequency_buffer, _size, _frequency_max, _color_buffer, _color_max, _palette };
            args_TestKernel = new CLCalc.Program.Variable[] { _test_result };

            UI.FormClosed += new FormClosedEventHandler(new Action<object, FormClosedEventArgs>((_sender, _e) =>
            {
                // if renderer is running
                if (ThreadRunning())
                    _evt_Abort();
            }));

            _evt_DrawImage += new OpenCLThr_EventHandler(() => {
                DrawImage.Execute(args_DrawImage, workers_DrawImage);
                // read the image from VRAM
                arg1.ReadFromDeviceTo(img);

                Synchronize(new Action(() => {
                    UI.DrawImage(ref bmp);
                }));
            });

            return true;
        }

        public bool CompileKernel()
        {
            try
            {
                // load and compile device code
                string kernel = File.ReadAllText(@"kernel/main.cl");
                CLCalc.Program.Compile(kernel);

                // load kernels
                buddhabrot = new CLCalc.Program.Kernel("main");
                DrawImage = new CLCalc.Program.Kernel("DrawImage");
                TestKernel = new CLCalc.Program.Kernel("TestKernel");

                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Iterate(_kernel_argv args, Random RNG)
        {
            args.ForEach((arg) =>
            {
                arg.Key.WriteToDevice((dynamic)arg.Value);
            });

            float[] random2 = new float[2];

            for (int i = 0; i < random2.Length; i++)
                random2[i] = Convert.ToSingle(RNG.NextDouble());
            _random2.WriteToDevice(random2);

            // execute buddhabrot kernel
            buddhabrot.Execute(args_buddhabrot, workers);

            return true;
        }

        public void Start()
        {
            // if renderer is already running
            if (ThreadRunning())
                return;

            OpenCLThr = new Thread(() =>
            {
                // safely stop the renderer if form is closed

                bool AbortShceduled = false;
                _evt_Abort += new OpenCLThr_EventHandler(() =>
                {
                    AbortShceduled = true;
                });

                Random rnd = new Random(0);

                stopWatch.Start();

                Action<int?> IterateWrapper = (frame) =>
                {
                    float power;

                    if (frame != null)
                        power = anim[(int)frame];
                    else
                        power = 4;

                    uint _maxiter;
                    if (anim.Length == 0)
                        _maxiter = (uint)maxiter;
                    else
                        _maxiter = (power < 2) ? Convert.ToUInt32(Math.Ceiling(maxiter / Math.Pow(power, 2))) : (power < 2.5f) ? Convert.ToUInt32(Math.Ceiling(maxiter / 4.0f / Math.Pow(power * 2 - 3, 2))) : 1024;
                    Random rnd2 = new Random(0);

                    for (uint j = 0; j < _maxiter; j++)
                    {
                        if (AbortShceduled)
                            break;

                        long t0 = stopWatch.ElapsedMilliseconds;

                        Iterate(new _kernel_argv {
                            new KeyValuePair<CLCalc.Program.Variable, object>( _anim, new float[] { power }),
                            new KeyValuePair<CLCalc.Program.Variable, object>( _iter, new uint[] { j, _maxiter }),
                        }, rnd2);

                        Synchronize(() =>
                        {
                            UI.UpdateUI(stopWatch.ElapsedMilliseconds, t0, j + 1, _maxiter, 0, (uint)anim.Length);
                        });

                        // execute DrawImage kernel
                        if (j % DrawInterval == 0)
                            _evt_DrawImage();

                        Thread.Sleep(1);
                    };
                };

                Action<int> Anim_SaveFrame = (frame) =>
                {
                    if (!AbortShceduled)
                    {
                        Synchronize(() =>
                        {
                            arg1.ReadFromDeviceTo(img);
                            bmp.Save(string.Format(@"anim/{0}.bmp", (frame + 1).ToString("0000")));
                        });
                        Thread.Sleep(400);
                        Reset();
                    }
                };

                for (int i = 0; i < random_cache.Length; i++)
                {
                    /*using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                    {
                        byte[] bytes = new byte[4];
                        rng.GetBytes(bytes);
                        random_cache[i] = BitConverter.ToUInt32(bytes, 0);
                    }*/
                    random_cache[i] = Convert.ToSingle(rnd.NextDouble());
                }
                _random_cache.WriteToDevice(random_cache);

                if(anim.Length == 0)
                    IterateWrapper(null);

                // animation mode
                else
                    for (int k = 0; k < anim.Length; k++)
                    {
                        if (AbortShceduled)
                            break;
                        IterateWrapper(k);
                        DrawImage.Execute(args_DrawImage, workers_DrawImage);
                        Anim_SaveFrame(k);
                    }

                stopWatch.Stop();
                _evt_DrawImage();
            });

            OpenCLThr.Start();
        }

        public void Reset()
        {
            Fill<uint>(ref img, 0);
            Fill<long>(ref frequency_buffer, 0);
            frequency_max = new long[] { 0 };
            Fill<float>(ref color_buffer, 0);
            color_max = 0;

            arg1.WriteToDevice(img);
            _frequency_buffer.WriteToDevice(frequency_buffer);
            _frequency_max.WriteToDevice(frequency_max);
            _color_buffer.WriteToDevice(color_buffer);
            _color_max.WriteToDevice(new float[] { color_max });

            _evt_DrawImage();
        }

        public static void Fill<T>(ref T[] destinationArray, params T[] value)
        {
            if (destinationArray == null)
            {
                throw new ArgumentNullException("destinationArray");
            }

            if (value.Length >= destinationArray.Length)
            {
                throw new ArgumentException("Length of value array must be less than length of destination");
            }

            // set the initial array value
            Array.Copy(value, destinationArray, value.Length);

            int arrayToFillHalfLength = destinationArray.Length / 2;
            int copyLength;

            for (copyLength = value.Length; copyLength < arrayToFillHalfLength; copyLength <<= 1)
            {
                Array.Copy(destinationArray, 0, destinationArray, copyLength, copyLength);
            }

            Array.Copy(destinationArray, 0, destinationArray, copyLength, destinationArray.Length - copyLength);
        }

        public bool ThreadRunning()
        {
            if ((OpenCLThr != null) && ((OpenCLThr.ThreadState == System.Threading.ThreadState.Running) || (OpenCLThr.ThreadState == System.Threading.ThreadState.WaitSleepJoin)))
                return true;
            return false;
        }

        void Synchronize(Action action)
        {
            if (UI.InvokeRequired)
                UI.BeginInvoke((MethodInvoker)delegate () { action(); });
            else
                action();
        }

        public string Test()
        {
            // execute test kernel
            TestKernel.Execute(args_TestKernel, new int[] { 1 });
            _test_result.ReadFromDeviceTo(test_result);
            return string.Format(
                "cpowr(2 + 1i, 2) = {0} + {1}i\n" +
                "cpow(2 + 1i, 2) = {2} + {3}i\n" +
                "cpow(3.7 + 0.5i, 2 + 1i) = {4} + {5}i\n" +
                "csqr(0.78 + 0.5i)@8 = {6} + {7}i\n" +
                "cpowr(0.78 + 0.5i, 2)@8 = {8} + {9}i\n" +
                "cpow((0.78 + 0.5i, 2)@8 = {10} + {11}i",
                test_result.Cast<object>().ToArray());
        }

        public bool UI_AbortAttempt()
        {
            if (!ThreadRunning())
                return false;
            _evt_Abort();
            return true;
        }
        public bool UI_CompileKernel()
        {
            if (CompileKernel())
            {
                _evt_DrawImage();
                return true;
            }
            return false;
        }
    }
}
